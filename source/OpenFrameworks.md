title: "OpenFrameworks"
date: 2017-04-02 19:30:00 +0800
update: 2017-04-02 19:30:00 +0800
author: me
cover: "-/images/pg.jpg"
tags:
    - OpenFrameworks
    - OpenCV
preview: As an **AOI and automation** engineer, I often deal with **OpenGL, OpenCV and other C + + library**.

---

As an **AOI and automation** engineer, I often deal with **OpenGL, OpenCV and other C + + library**. These libraries having different rules and usage; in addition, the preparation of the interface is also a routine work in order to make an own programs with more interactive capabilities. However, with the increase of engineering complexity, the management and maintenance of the library becomes more and more difficult. On other hand, the addition and deletion of the library will not only increase the learning cost, but also affect the logical layer of the entire system. so, if you want to make your project easy to maintain, it is necessary to use the design pattern as much as possible, so that the separation of logic and interface will more easy to maintain. But for scientific research, blindly into the shackles of design patterns will lead to premature optimization of the problem, affecting the progress of scientific research. Until then, I came across with **openFrameworks**.

## **What is openFrameworks**
openFrameworks is an open source C++ toolkit designed to assist the creative process by providing a simple and intuitive framework for experimentation.

openFrameworks is designed to work as a general purpose glue, and wraps together several commonly used libraries, including:

- [OpenGL], [GLEW], [GLUT], [libtess2] and [cairo] for graphics
- [rtAudio], [PortAudio], [OpenAL] and [Kiss FFT] or [FMOD] for audio input, output and analysis
- [FreeType] for fonts
- [FreeImage] for image saving and loading
- [Quicktime], [GStreamer] and [videoInput] for video playback and grabbing
- [Poco] for a variety of utilities
- [OpenCV] for computer vision
- [Assimp] for 3D model loading

While these libraries follow different rules and usage, oF provides a common interface on top of them, making it easy to use them.

In addition, oF Another bright spot is that it has a good cross-platform features. At present, it supports five operating systems (Windows, OSX, Linux, iOS, Android) and four integrated development environment (XCode, qt creator, Visual Studio, Eclipse).

## **Install and configure openFrameworks**
The following describes how to install and configure oF under Linux.

### **Download openFrameworks**
Visit [oF's official download page] and find the version that applies to your operating system and IDE. Click Download. For example, my computer is a Linux Ubuntu 64-bit system and qt creator is my IDE, so I chose **[Linux 64bit]**, and the setup guide of **[qt creator]**.

### **Install Dependencies**
Download is complete, extract it, open the terminal, `cd` to extract the directory, for example:

```
$ /home/thyeun/OF
```

After that, depending on your Linux distribution, `cd` into scripts / linux / <*operating system release name*>, for example:

```
$ cd scripts/linux/ubuntu
```

Execute two commands for install openFrameworks dependencies (requires root privileges):

```
$ sudo ./install_dependencies.sh
$ sudo ./install_codecs.sh
```

### **Compiler oF**
After installing dependencies, return to the previous directory:

```
$ cd ..
```
Execute compiler oF:

```
$ ./compileOF.sh
```

### **Compile the project builder**
In order to facilitate the creation of future projects, oF also provides a project generator projectGenerator. It is also required to compile before using it. Go back to the directory where the `compileOF.sh` script is located and type the following command:

```
$ ./compilePG.sh
```
After the completion of installation of `compilePG.sh`,  go back to the root directory of oF, and look for projectGenerator directory, which can be found inside the projectGenerator, we can implement it:

```
$ cd ../../projectGenerator
$ ./projectGenerator
```
![image](-/images/pg.jpg)

Click on the bottom left corner of the **GENERATE** button, it will specified the path to generate a project path, as shown above picture is / home / thyeun / OF / apps / myApps / mySketch:

```
$ /home/thyeun/OF/apps/myApps/mySketch
$ ls
addons.make  bin  config.make  Makefile  mySketch.qbs  mySketch.qbs.user  src
```
among them:

- `Addons.make` file - a list of plug-ins that are used to maintain this project;

- `Config.make` file - used to add a search path, modify the optimization tag, and other settings;

- `Makefile` - Makefile of the project, generally do not need to directly modify it.

    In oF, The goals of make include:

   - **Debug**: Generates an executable program with debugging flags;

   - **Release**: generates compiler-optimized executables;

   - **Clean**: clear target files and executable programs;

   - **CleanDebug**: only clear the debug target of the generated results;

   - **CleanRelease**: only release the release target of the generated results;

   - **Help**: print help information;

   - **Run**: Executes the generated executable.

- `MySketch.qbs` and `mySketch.qbs.user` file - qt creator project files.

### **Template installation for qt Creator**
After finish the installation of dependencies and compiler, than `cd` to scripts / qtcreator, for install qt creator template.

```
$ cd scripts/qtcreator
```
Execute the commands for the template installation for qt creator

```
$ sudo ./install_template.sh
```



[OpenGL]: http://www.opengl.org/
[GLEW]: http://glew.sourceforge.net/
[GLUT]: http://www.opengl.org/resources/libraries/glut/
[libtess2]: https://code.google.com/p/libtess2/
[cairo]: http://cairographics.org/
[rtAudio]: http://www.music.mcgill.ca/~gary/rtaudio/
[PortAudio]: http://www.portaudio.com/
[OpenAL]: http://http//connect.creativelabs.com/openal
[Kiss FFT]: http://kissfft.sourceforge.net/
[FMOD]: http://www.fmod.org/
[FreeType]: http://freetype.sourceforge.net/index2.html
[FreeImage]: http://freeimage.sourceforge.net/
[Quicktime]: http://developer.apple.com/quicktime/
[GStreamer]: http://gstreamer.freedesktop.org/
[videoInput]: https://github.com/ofTheo/videoInput
[Poco]: http://pocoproject.org/
[OpenCV]: http://opencv.org/
[Assimp]: http://assimp.sourceforge.net/
[oF's official download page]: http://openframeworks.cc/download/
[Linux 64bit]: http://openframeworks.cc/versions/v0.9.6/of_v0.9.6_linux64_release.tar.gz
[qt creator]: http://openframeworks.cc/setup/qtcreator
