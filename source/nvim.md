title: "NeoVim"
date: 2018-06-07 18:46:00 +0800
author: me
cover: "-/images/neovim.png"
tags:
    - nvim
    - neovim
preview: Neovim

---

## **Neovim**

Neovim is a refactor, and sometimes redactor, in the tradition of Vim (which itself derives from Stevie). It is not a rewrite but a continuation and extension of Vim. Many clones and derivatives exist, some very clever—but none are Vim. Neovim is built for users who want the good parts of Vim, and more.

**Goals**

   - Flexible, extensible Vim with a first-class, fast scripting alternative (Lua/LuaJIT)
   - Consistent user experience across platforms
   - Leverage ongoing Vim development: harmony
   - Maintain feature parity with Vim; avoid regressions
   - Continue the Vim tradition of backwards compatibility, with few exceptions
   - Keep the core small and fast
   - Target all platforms supported by libuv
   - Optimize out of the box, for new users but especially regular users
   - Delegate to plugins, but preserve the utility of the editor core

**Non-Goals**

   - Turn Vim into an IDE
   - Limit third-party applications (such as IDEs!) built with Neovim
   - Deprecate VimL
   - Vi-compatibility

**Principles**

   - Do not regress from origin
   - Decide outcomes by weighing cost and benefit
   - Use automation to solve problems
   - Enable new contributors: remove barriers to entry
   - Unblock third parties and plugin authors: allow progress
   - In matters of taste or ambiguity, favor tradition/compatibility...
   - ...but prefer usability over tradition if the benefits are overwhelming
   - Give usability a chance™

Below is my Neovim Gui and a running Machine Learning on Neovim.

![Neovim Gui](-/images/nvimGUI.jpg)
![Neovim with Machine Learning](-/images/nvimML.jpg)


