title: "Atom-IDE"
date: 2018-07-02 07:38:35 +0800
update: 2018-07-02 08:33:20 +0800
author: me
cover: "-/images/Atom-editor.png"
tags:
    - Atom
    - Atom Editor
    - Atom-IDE
preview: The hackable text editor.

---

**Atom** is a free and open-source text and source code editor for **macOS**, **Linux**, and **Microsoft Windows** with support for plug-ins written in **Node.js**, and embedded Git Control, developed by **GitHub**. **Atom** is a desktop application built using web technologies. Most of the extending packages have free software licenses and are community-built and maintained. **Atom** is based on **Electron** (formerly known as Atom Shell), a framework that enables cross-platform desktop applications using Chromium and **Node.js**. It is written in **CoffeeScript** and Less. It can also be used as an integrated development environment (IDE). Atom was released from beta, as version 1.0, on 25 June 2015. Its developers call it a **"hackable text editor for the 21st Century"**.

Using the default plugins, the following programming languages are supported in some aspect as of v1.28.0:

**[C/C++]**, **[C#]**, **[Clojure]**, **[COBOL]**, **[CSS]**, **[CoffeeScript]**, **[GitHub Flavored Markdown]**, **[Go]**, **[Git]**, **[HTML]**, **[JavaScript]**, **[Java]**, **[JSON]**, **[Julia]**, **[Less]**, **[Make]**, **[Mustache]**, **[Objective-C]**, **[PHP]**, **[Perl]**, **[Property List (Apple)]**, **[Python]**, **[Ruby on Rails]**, **[Ruby]**, **[Sass]**, **[Shell script]**, **[Scala]**, **[SQL]**, **[TOML]**, **[XML]**, **[YAML]**

![Atom Editor](-/images/Atom_icon.png)

[C/C++]:                      https://en.wikipedia.org/wiki/C%2B%2B
[C#]:                         https://en.wikipedia.org/wiki/C_Sharp_(programming_language)
[Clojure]:                    https://en.wikipedia.org/wiki/Clojure_(programming_language)
[COBOL]:                      https://en.wikipedia.org/wiki/COBOL
[CSS]:                        https://en.wikipedia.org/wiki/CSS
[CoffeeScript]:               https://en.wikipedia.org/wiki/CoffeeScript
[GitHub Flavored Markdown]:   https://en.wikipedia.org/wiki/GitHub_Flavored_Markdown
[Go]:                         https://en.wikipedia.org/wiki/Go_(programming_language)
[Git]:                        https://en.wikipedia.org/wiki/Git
[HTML]:                       https://en.wikipedia.org/wiki/HTML
[JavaScript]:                 https://en.wikipedia.org/wiki/JavaScript
[Java]:                       https://en.wikipedia.org/wiki/Java_(programming_language)
[JSON]:                       https://en.wikipedia.org/wiki/JSON
[Julia]:                      https://en.wikipedia.org/wiki/Julia_(programming_language)
[Less]:                       https://en.wikipedia.org/wiki/Less_(stylesheet_language)
[Make]:                       https://en.wikipedia.org/wiki/Make_(software)
[Mustache]:                   https://en.wikipedia.org/wiki/Mustache_(template_system)
[Objective-C]:                https://en.wikipedia.org/wiki/Objective-C
[PHP]:                        https://en.wikipedia.org/wiki/PHP
[Perl]:                       https://en.wikipedia.org/wiki/Perl
[Property List (Apple)]:      https://en.wikipedia.org/wiki/Property_list
[Python]:                     https://en.wikipedia.org/wiki/Python_(programming_language)
[Ruby on Rails]:              https://en.wikipedia.org/wiki/Ruby_on_Rails     
[Ruby]:                       https://en.wikipedia.org/wiki/Ruby_(programming_language)
[Sass]:                       https://en.wikipedia.org/wiki/Sass_(stylesheet_language)
[Shell script]:               https://en.wikipedia.org/wiki/Shell_script
[Scala]:                      https://en.wikipedia.org/wiki/Scala_(programming_language)
[SQL]:                        https://en.wikipedia.org/wiki/SQL
[TOML]:                       https://en.wikipedia.org/wiki/TOML
[XML]:                        https://en.wikipedia.org/wiki/XML
[YAML]:                       https://en.wikipedia.org/wiki/YAML
