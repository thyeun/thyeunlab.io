title: "Visual Studio Code"
date: 2018-07-31 07:38:35 +0800
update:
author: me
cover: "-/images/vscode.png"
tags:
    - Visual Studio Code
    - VS Code
    - Editor
preview: A free cross platform code editor.

---

**Visual Studio Code** is a source code editor developed by **Microsoft** for **Windows**, **Linux** and **macOS**. It includes support for **debugging**, **embedded Git control**, **syntax highlighting**, **intelligent code completion**, **snippets**, and **code refactoring**. It is also customizable, so users can change the editor's theme, keyboard shortcuts, and preferences. It is free and open-source, although the official download is under a proprietary license.

Visual Studio Code is based on **Electron**, a framework which is used to deploy **Node.js** applications for the desktop running on the Blink layout engine. Although it uses the Electron framework, the software does not use Atom and instead employs the same editor component (codenamed "Monaco") used in **Visual Studio Team Services** (formerly called **Visual Studio Online**).

<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg .tg-l711{border-color:inherit}
.tg .tg-us36{border-color:inherit;vertical-align:top}
</style>
<table class="tg">
  <tr>
    <th class="tg-l711">Language</th>
    <th class="tg-l711">Snippets</th>
    <th class="tg-l711">Syntax highlighting</th>
    <th class="tg-us36">Brace matching</th>
    <th class="tg-us36">Code folding</th>
  </tr>
  <tr>
    <td class="tg-l711">C and C++</td>
    <td class="tg-l711" align=center>Yes</td>
    <td class="tg-l711" align=center>Yes</td>
    <td class="tg-us36" align=center>Yes</td>
    <td class="tg-us36" align=center>Yes</td>
  </tr>
  <tr>
    <td class="tg-l711">C#</td>
    <td class="tg-l711" align=center>Yes</td>
    <td class="tg-l711" align=center>Yes</td>
    <td class="tg-us36" align=center>Yes</td>
    <td class="tg-us36" align=center>Yes</td>
  </tr>
  <tr>
    <td class="tg-l711">Clojure</td>
    <td class="tg-l711" align=center>No</td>
    <td class="tg-l711" align=center>Yes</td>
    <td class="tg-us36" align=center>Yes</td>
    <td class="tg-us36" align=center>No</td>
  </tr>
  <tr>
    <td class="tg-us36">CoffeeScript</td>
    <td class="tg-l711" align=center>Yes</td>
    <td class="tg-l711" align=center>Yes</td>
    <td class="tg-us36" align=center>Yes</td>
    <td class="tg-us36" align=center>Yes</td>
  </tr>
  <tr>
    <td class="tg-us36">CSS</td>
    <td class="tg-l711" align=center>No</td>
    <td class="tg-l711" align=center>Yes</td>
    <td class="tg-us36" align=center>Yes</td>
    <td class="tg-us36" align=center>No</td>
  </tr>
  <tr>
    <td class="tg-us36">Dockerfile</td>
    <td class="tg-l711" align=center>No</td>
    <td class="tg-l711" align=center>Yes</td>
    <td class="tg-us36" align=center>Yes</td>
    <td class="tg-us36" align=center>No</td>
  </tr>
  <tr>
    <td class="tg-us36">F#</td>
    <td class="tg-l711" align=center>Yes</td>
    <td class="tg-l711" align=center>Yes</td>
    <td class="tg-us36" align=center>Yes</td>
    <td class="tg-us36" align=center>Yes</td>
  </tr>
  <tr>
    <td class="tg-us36">Go</td>
    <td class="tg-l711" align=center>No</td>
    <td class="tg-l711" align=center>Yes</td>
    <td class="tg-us36" align=center>Yes</td>
    <td class="tg-us36" align=center>No</td>
  </tr>
  <tr>
    <td class="tg-us36">Groovy</td>
    <td class="tg-l711" align=center>Yes</td>
    <td class="tg-l711" align=center>Yes</td>
    <td class="tg-us36" align=center>Yes</td>
    <td class="tg-us36" align=center>No</td>
  </tr>
  <tr>
    <td class="tg-us36">Handlebars</td>
    <td class="tg-l711" align=center>No</td>
    <td class="tg-l711" align=center>Yes</td>
    <td class="tg-us36" align=center>Yes</td>
    <td class="tg-us36" align=center>No</td>
  </tr>
  <tr>
    <td class="tg-us36">HLSL</td>
    <td class="tg-l711" align=center>No</td>
    <td class="tg-l711" align=center>Yes</td>
    <td class="tg-us36" align=center>Yes</td>
    <td class="tg-us36" align=center>No</td>
  </tr>
  <tr>
    <td class="tg-us36">HTML/HTML5</td>
    <td class="tg-l711" align=center>Yes</td>
    <td class="tg-l711" align=center>Yes</td>
    <td class="tg-us36" align=center>Yes</td>
    <td class="tg-us36" align=center>No</td>
  </tr>
  <tr>
    <td class="tg-us36">INI file</td>
    <td class="tg-l711" align=center>No</td>
    <td class="tg-l711" align=center>Yes</td>
    <td class="tg-us36" align=center>Yes</td>
    <td class="tg-us36" align=center>No</td>
  </tr>
  <tr>
    <td class="tg-us36">Java</td>
    <td class="tg-l711" align=center>Yes</td>
    <td class="tg-l711" align=center>Yes</td>
    <td class="tg-us36" align=center>Yes</td>
    <td class="tg-us36" align=center>Yes</td>
  </tr>
  <tr>
    <td class="tg-us36">JavaScript</td>
    <td class="tg-l711" align=center>Yes</td>
    <td class="tg-l711" align=center>Yes</td>
    <td class="tg-us36" align=center>Yes</td>
    <td class="tg-us36" align=center>Yes</td>
  </tr>
  <tr>
    <td class="tg-us36">JSON</td>
    <td class="tg-l711" align=center>No</td>
    <td class="tg-l711" align=center>Yes</td>
    <td class="tg-us36" align=center>Yes</td>
    <td class="tg-us36" align=center>No</td>
  </tr>
  <tr>
    <td class="tg-us36">LESS</td>
    <td class="tg-l711" align=center>No</td>
    <td class="tg-l711" align=center>Yes</td>
    <td class="tg-us36" align=center>Yes</td>
    <td class="tg-us36" align=center>Yes</td>
  </tr>
  <tr>
    <td class="tg-us36">Log file	</td>
    <td class="tg-l711" align=center>No</td>
    <td class="tg-l711" align=center>Yes</td>
    <td class="tg-us36" align=center>No</td>
    <td class="tg-us36" align=center>No</td>
  </tr>
  <tr>
    <td class="tg-us36">Lua</td>
    <td class="tg-l711" align=center>No</td>
    <td class="tg-l711" align=center>Yes</td>
    <td class="tg-us36" align=center>Yes</td>
    <td class="tg-us36" align=center>No</td>
  </tr>
  <tr>
    <td class="tg-us36">Makefile</td>
    <td class="tg-l711" align=center>No</td>
    <td class="tg-l711" align=center>Yes</td>
    <td class="tg-us36" align=center>Yes</td>
    <td class="tg-us36" align=center>No</td>
  </tr>
  <tr>
    <td class="tg-us36">Markdown</td>
    <td class="tg-l711" align=center>Yes</td>
    <td class="tg-l711" align=center>Yes</td>
    <td class="tg-us36" align=center>No</td>
    <td class="tg-us36" align=center>No</td>
  </tr>
  <tr>
    <td class="tg-us36">Objective-C	</td>
    <td class="tg-l711" align=center>No</td>
    <td class="tg-l711" align=center>Yes</td>
    <td class="tg-us36" align=center>Yes</td>
    <td class="tg-us36" align=center>No</td>
  </tr>
  <tr>
    <td class="tg-us36">Perl</td>
    <td class="tg-l711" align=center>No</td>
    <td class="tg-l711" align=center>Yes</td>
    <td class="tg-us36" align=center>Yes</td>
    <td class="tg-us36" align=center>No</td>
  </tr>
  <tr>
    <td class="tg-us36">PHP</td>
    <td class="tg-l711" align=center>No</td>
    <td class="tg-l711" align=center>Yes</td>
    <td class="tg-us36" align=center>Yes</td>
    <td class="tg-us36" align=center>No</td>
  </tr>
  <tr>
    <td class="tg-us36">Powershell</td>
    <td class="tg-l711" align=center>Yes</td>
    <td class="tg-l711" align=center>Yes</td>
    <td class="tg-us36" align=center>Yes</td>
    <td class="tg-us36" align=center>Yes</td>
  </tr>
  <tr>
    <td class="tg-us36">Pug JS</td>
    <td class="tg-l711" align=center>No</td>
    <td class="tg-l711" align=center>Yes</td>
    <td class="tg-us36" align=center>Yes</td>
    <td class="tg-us36" align=center>No</td>
  </tr>
  <tr>
    <td class="tg-us36">Python</td>
    <td class="tg-l711" align=center>No</td>
    <td class="tg-l711" align=center>Yes</td>
    <td class="tg-us36" align=center>Yes</td>
    <td class="tg-us36" align=center>Yes</td>
  </tr>
  <tr>
    <td class="tg-us36">R</td>
    <td class="tg-l711" align=center>No</td>
    <td class="tg-l711" align=center>Yes</td>
    <td class="tg-us36" align=center>Yes</td>
    <td class="tg-us36" align=center>No</td>
  </tr>
  <tr>
    <td class="tg-us36">Razor</td>
    <td class="tg-l711" align=center>No</td>
    <td class="tg-l711" align=center>Yes</td>
    <td class="tg-us36" align=center>Yes</td>
    <td class="tg-us36" align=center>Yes</td>
  </tr>
  <tr>
    <td class="tg-us36">Ruby</td>
    <td class="tg-l711" align=center>No</td>
    <td class="tg-l711" align=center>Yes</td>
    <td class="tg-us36" align=center>Yes</td>
    <td class="tg-us36" align=center>No</td>
  </tr>
  <tr>
    <td class="tg-us36">Rust</td>
    <td class="tg-l711" align=center>No</td>
    <td class="tg-l711" align=center>Yes</td>
    <td class="tg-us36" align=center>Yes</td>
    <td class="tg-us36" align=center>No</td>
  </tr>
  <tr>
    <td class="tg-us36">SCSS</td>
    <td class="tg-l711" align=center>No</td>
    <td class="tg-l711" align=center>Yes</td>
    <td class="tg-us36" align=center>Yes</td>
    <td class="tg-us36" align=center>Yes</td>
  </tr>
  <tr>
    <td class="tg-us36">Shaderlab</td>
    <td class="tg-l711" align=center>No</td>
    <td class="tg-l711" align=center>Yes</td>
    <td class="tg-us36" align=center>Yes</td>
    <td class="tg-us36" align=center>No</td>
  </tr>
  <tr>
    <td class="tg-us36">Shell script</td>
    <td class="tg-l711" align=center>No</td>
    <td class="tg-l711" align=center>Yes</td>
    <td class="tg-us36" align=center>Yes</td>
    <td class="tg-us36" align=center>No</td>
  </tr>
  <tr>
    <td class="tg-us36">SQL</td>
    <td class="tg-l711" align=center>No</td>
    <td class="tg-l711" align=center>Yes</td>
    <td class="tg-us36" align=center>Yes</td>
    <td class="tg-us36" align=center>No</td>
  </tr>
  <tr>
    <td class="tg-us36">Swift</td>
    <td class="tg-l711" align=center>Yes</td>
    <td class="tg-l711" align=center>Yes</td>
    <td class="tg-us36" align=center>Yes</td>
    <td class="tg-us36" align=center>No</td>
  </tr>
  <tr>
    <td class="tg-us36">TypeScript</td>
    <td class="tg-l711" align=center>Yes</td>
    <td class="tg-l711" align=center>Yes</td>
    <td class="tg-us36" align=center>Yes</td>
    <td class="tg-us36" align=center>Yes</td>
  </tr>
  <tr>
    <td class="tg-us36">Visual Basic</td>
    <td class="tg-l711" align=center>Yes</td>
    <td class="tg-l711" align=center>Yes</td>
    <td class="tg-us36" align=center>Yes</td>
    <td class="tg-us36" align=center>Yes</td>
  </tr>
  <tr>
    <td class="tg-us36">WASM</td>
    <td class="tg-l711" align=center>-</td>
    <td class="tg-l711" align=center>-</td>
    <td class="tg-us36" align=center>-</td>
    <td class="tg-us36" align=center>-</td>
  </tr>
  <tr>
    <td class="tg-us36">Windows batch file</td>
    <td class="tg-l711" align=center>Yes</td>
    <td class="tg-l711" align=center>Yes</td>
    <td class="tg-us36" align=center>Yes</td>
    <td class="tg-us36" align=center>Yes</td>
  </tr>
  <tr>
    <td class="tg-us36">XML</td>
    <td class="tg-l711" align=center>No</td>
    <td class="tg-l711" align=center>Yes</td>
    <td class="tg-us36" align=center>Yes</td>
    <td class="tg-us36" align=center>No</td>
  </tr>
  <tr>
    <td class="tg-us36">YAML</td>
    <td class="tg-l711" align=center>No</td>
    <td class="tg-l711" align=center>Yes</td>
    <td class="tg-us36" align=center>Yes</td>
    <td class="tg-us36" align=center>No</td>
  </tr>
</table>

![Visual Studio Code](-/images/vscode_icon.png)