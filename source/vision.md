title: "Vision Solution Principles"
date: 2017-03-21 14:00:00 +0800
update: 2017-03-21 14:00:00 +0800
author: me
cover: "-/images/vision.JPG"
tags:
    - Vision
    - AOI
preview: Vision Solution Principles

---

## **Vision Solution Principles**
Choosing and implementing machine vision technology involves the following questions:

1. Is vision needed to do the job?
2. Is there a financial incentive for investing in machine vision?
3. Is the application solvable with vision?
4. Which vision technology should be used?
5. What does a typical vision project look like?
6. What problems might be encountered along the way?

![Vision Solution](-/images/vision.JPG)

### **Standard Sensors**
Vision is a powerful and interesting technology, but far from always
the best solution. It is important to keep in mind the vast possibilities with standard sensors and also the option of combining cameras with standard sensors. A simple solution that works is a preferable solution.

### **Vision Qualifier**
When assessing the suitability of an application to be solved by machine vision, there are
certain economic and technical key issues to consider.

#### **Investment Incentive**
Vision systems are seldom off-the-shelf products ready for plug-and-play installation, more often they should be considered as project investments. The reason is that vision solutions almost always involve some level of programming and experimenting before the application is robust and operational.
The first step is thus to determine if there is a financial incentive or justification for an
investment. There are four main incentives for this investment:

1. Reduced cost of labor: Manual labor is often more costly than vision systems.

2. Increase in production yield: The percentage of the produced products that are
judged as good-enough to be sold.

3. Improved and more even product quality: The actual quality of the sold products
through more accurate inspections. Even a skilled inspector can get tired and let
through a defect product after some hours of work.

4. Increase in production speed: The output can be increased wherever manual in-
spections are a bottleneck in the production.
The price of the vision system should be put in perspective of the investment incentive, i.e.
the combined effect of reduced labor and increase in yield, quality, and production speed.
A rule of thumb is that the total cost of low-volume applications are approximately twice
the hardware price, including the cost of integration.
Once the financial incentive is defined, a feasibility study can be considered.

#### **Application Solvability**
**Image quality**

Having good contrast conditions and high-enough resolution is essential. A poor or variable
image quality can sometimes be compensated for by the use of algorithms, but developing
them is costly and robustness is an issue. In general, it is worthwhile to strive towards the
best-possible image quality before going on to the image processing.

A key factor in building a robust vision application is to obtain good **repeatability of object
representation regarding**:

1. Illumination

2. Object location and rotation

There are methods to deal with variations in these factors, but in general less variability
gives a more robust application. The optimal situation is a shrouded inspection station
with constant illumination where the object has a fixed position and rotation relative to the
camera.

**Image Processing Algorithms**

Having a good image addresses the first half of the solution. The next step is to apply
image processing algorithms or tools to do the actual analysis. Some vision systems are
delivered with a ready-to-use software or toolbox, whereas others need third-party algo-
rithms or even custom development of algorithms. This can have a heavy impact on the
project budget.

### **Vision Project Parts**

Once the application has qualified as a viable vision project, the phases that follow are:
Feasibility study, investment, implementation, and acceptance testing.

#### **Feasibility Study**

The purpose of a **feasibility study** is to determine if the problem can
be solved with vision or not. In the feasibility **report**, there is information about the application, what parts have been solved and how, and which problems and challenges can be expected if the application
becomes a project.
The feasibility study should either reach **proof-of-concept** (meaning "Yes, we can solve the
application"), identify why the application is not solvable, or state which further information
is needed before proof-of-concept can be reached.

#### **Investment**

Once the feasibility study is complete, it's time for the **investment decision** and the **project definition**. The project definition should contain a full description of what the vision system shall do and how it will perform. A procedure for acceptance testing should be included.

#### **Implementation**

The **implementation** is the practical work of building the system. The contents and extent of the implementation phase may vary from a partial to a complete solution. Implementation is often called **integration**. When a company provides integration services, they are referred to as a **system integrator**. When vision is the integrator’s main business area, they are referred to as a vision integrator.

#### **Commissioning and Acceptance Testing**

Once the implementation phase is completed, it is time for **commissioning** of the system, or handing it over to the customer. A part of the commissioning is an **acceptance test** according to the procedure
described in the project definition.

The acceptance test description contains clear conditions of customer expectations of the
system. If the test is passed, the system is considered to be completed or delivered.

### **Application Solving Method**

The general method for solving a vision application consists of the following steps: Defining the task, choosing hardware, choosing image processing tools, defining a result output, and testing the application. Some iteration is usually needed before a final solution is reached.

#### **Defining the Task**

Defining the task is essentially to describe exactly what the vision system shall do, which
performance is expected, and under which circumstances.

It is instrumental to have **samples** and knowledge about the industrial site where the
system will be located. The collection of samples needs to be representative for the full
object variation, for example including good, bad, and limit cases.

In defining the task, it is important to decide how the inspected features can be param-
eterized to reach the desired final result.

#### **Choosing Hardware**

The steps for selecting system hardware are:

1. The type of object and inspection determine the choice of camera technology.

2. The object size and positioning requirements determine the FOV.

3. The smallest feature to be detected and the FOV size determine the resolution.

4. The FOV and the object distance determine the lens' focal length (See the Ap-
pendix for explanations and example calculations of needed focal length.)

5. The type of inspections and the object's environment determine the choice of
lighting.

6. The type of result to be delivered (digital I/O, Ethernet, etc) determines the choice
of cables and other accessories.

7. The choice of camera and lighting determines the mounting mechanics.
The above list is just a rough outline. Arriving at a well-defined solution requires practical
experience and hands-on testing.
When standard hardware is not sufficient for the task, a customization might be needed.

#### **Choosing Image Processing Tools**

In choosing the processing tools for a certain application, there are often a number of
possibilities and combinations. How to choose the right one? This requires practical experience and knowledge about the available tools for the camera system at hand.

#### **Defining a Result Output**

The next step is to define how to **communicate the result**, for example to a PLC, a data-
base, or a sorting machine. The most common output in machine vision is pass/fail.

#### **Testing the Application**

The application is not finished until it has been tested, debugged, and pushed to its limits.
This means that the system function must be tested for normal cases as well as a number
of less frequent but possible cases, for example:

1. Ambient light fluctuations, reflections, sunshine through a window, etc.

2. Close to the acceptance limit for good and bad.

3. The extremes of accepted object movement in the FOV.


### **Challenges**

During the feasibility study and implementation of a vision application there are some challenges
that are more common than others.

This section treats typical bottlenecks and pitfalls
in vision projects.

#### **Defining Requirements**

It can be a challenge to define the task so that all involved parties have the same expectations of system performance. The customer has the perspective and terminology of his or her industry, and so does the vision supplier. Communication between both parties may require that each share their knowledge. To formalize clear **acceptance test conditions** is a good way of communicating the expectations of the system.

#### **Performance**

The **cycle time** can become a critical factor in the choice of camera system and algorithms
when objects are inspected at a high frequency. This situation is typical for the packaging
and pharmaceutical industries.

**Accuracy** is the repeatability of measurements as compared to a reference value or
position (measure applications). 

**Success rate** is the system’s reliability in terms of **false OKs** and **false rejects** (inspect
and identify applications). A false OK is when a faulty object is wrongly classified as OK,
and a false reject is when an OK object is wrongly classified as false. It is often important
to distinguish between the two aspects, since the consequences of each can be totally
different in the production line.

#### **System Flexibility**

Building a vision system that performs one task in a constant environment can be easy.
However, the system’s **complexity** can increase significantly when it shall inspect variable
objects in a variable environment.

Worth keeping in mind is that objects that are very similar in the mind of their producer
can be totally different from a vision perspective. It is common to expect that since the
vision system inspects object A with such success, it must also be able to inspect objects
B and C with the same setup since they are “so similar”.

#### **Object Presentation Repeatability**

The **object presentation** is the object’s appearance in the image, including position,
rotation, and illumination. With high repeatability in the image, the application solving can
be easy. On the other hand, the application solving may become difficult or even impossi-
ble for the very same object if its presentation is arbitrary.

For example, **rotation invariance** (360 degree rotation tolerance) in a 2D application is
more demanding on the processing than a non-rotating object. In 3D, rotation invariance
might not even be possible for some objects because of occluded features.

#### **Mechanics and Environment**

Although a vision system can be a technically optimal solution, sometimes there is not
enough **mounting space**. Then one must consider an alternative solution or redesign of
the machine.

Some industries have environments with **heat, vibrations, dust, and humidity concerns**.
Such conditions can have undesirable side-effects: Reduced hardware performance, life,
and deteriorated image quality (blur). Information about the hardware's ability to withstand
such conditions is found in its technical specifications.