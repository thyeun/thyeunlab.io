title: "Eclipse Che indicator"
date: 2017-04-26 00:10:00 +0800
update: 2017-04-26 00:13:00 +0800
author: me
cover: "-/images/che.jpg"
tags:
    - Eclipse Che
    - Che
preview: Eclipse Che is a developer workspace server and cloud IDE. Install, run, and manage Eclipse Che with Docker.

---

![Eclipse Che](-/images/che.jpg)
Create a simple indicator that execute "**Start che**", "**Stop che**" and running "**Eclipse Che**".

Using python to create a simple indicator on **ubuntu 16.04**.

Below is the source code:

```
import os
import signal
import json
import subprocess
import gi

gi.require_version('Gtk', '3.0')
gi.require_version('AppIndicator3', '0.1')

from gi.repository import Gtk as gtk
from gi.repository import AppIndicator3 as appindicator

APPINDICATOR_ID = 'myappindicator'

def main():
    indicator = appindicator.Indicator.new(APPINDICATOR_ID, os.path.abspath('/home/thyeun/Pictures/type-che.png'), appindicator.IndicatorCategory.SYSTEM_SERVICES)
    indicator.set_status(appindicator.IndicatorStatus.ACTIVE)
    indicator.set_menu(build_menu())
    gtk.main()

def build_menu():
    menu = gtk.Menu()
    item_start = gtk.MenuItem('Start Che')
    item_start.connect('activate', start)
    menu.append(item_start)
    item_stop = gtk.MenuItem('Stop Che')
    item_stop.connect('activate', stop)
    menu.append(item_stop)
    item_che = gtk.MenuItem('Eclipse Che')
    item_che.connect('activate', che)
    menu.append(item_che)					
    item_quit = gtk.MenuItem('Quit')
    item_quit.connect('activate', quit)
    menu.append(item_quit)
    menu.show_all()
    return menu

def start(_):
    subprocess.call('/home/thyeun/Desktop/che-start.sh')
    return start	

def stop(_):
    subprocess.call('/home/thyeun/Desktop/che-stop.sh')
    return stop

def che(_):
    os.system('/opt/google/chrome/chrome --profile-directory=Default --app-id=cagdgppejagmigblhodalodfhllclleg')	
    return che

def quit(_):
    gtk.main_quit()

if __name__ == "__main__":
    signal.signal(signal.SIGINT, signal.SIG_DFL)
    main()
```
Save the file as **```che-indicator.py```**

- Create a **```che-start.sh```**

```
#!/bin/bash

sudo docker run -it --rm -v /var/run/docker.sock:/var/run/docker.sock -v /home/thyeun/che:/data eclipse/che start 

```
**Change the folder name according to your own system**.

- Create also **```che-stop.sh```**

```
#!/bin/bash

sudo docker run -it --rm -v /var/run/docker.sock:/var/run/docker.sock -v /home/thyeun/che:/data eclipse/che stop 

```
**Change the folder name according to your own system**.

- Using Chrome browser make a localhost Eclipse Che desktop apps.

```
#!/usr/bin/env xdg-open

[Desktop Entry]
Version=1.0
Terminal=false
Type=Application
Name=Eclipse Che
Exec=/opt/google/chrome/chrome --profile-directory=Default --app-id=cagdgppejagmigblhodalodfhllclleg
Icon=/home/thyeun/Pictures/type-che.png
StartupWMClass=crx_cagdgppejagmigblhodalodfhllclleg
Name[en_US]=Eclipse

```
**```Exec```** are the directory that you using chrome browser to create the desktop apps.

Finally, this will be the outcome once you compile the **```che-indicator.py```**.

![Che indicator](-/images/che-indicator.jpg)

![Che Menu](-/images/che-menu.jpg)

![Che launcher](-/images/che-launcher.jpg)

Hope it help.
