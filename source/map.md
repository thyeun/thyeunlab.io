title: "SOHO"
date: 2017-03-21 23:00:00 +0800
update: 2017-03-21 23:00:00 +0800
author: me
cover: "-/images/maps.jpg"
tags:
    - SOHO
    - Jalan Ong Kim Wee
preview: Jalan Ong Kim Wee Melaka Malaysia

---

<center>
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3369.1138916035557!2d102.24360954062189!3d2.2022889644971753!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31d1f1d0016043b1%3A0x185c02fcad88c179!2sJalan+Ong+Kim+Wee%2C+75300+Melaka!5e1!3m2!1sen!2smy!4v1490128977896" width="650" height="400" frameborder="0" style="border:0" allowfullscreen>
</iframe>
</center>
